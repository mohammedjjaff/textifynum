import 'package:textifynum/textifynum.dart';
import 'package:test/test.dart';

void main() {
  group('A group of tests', () {
    final awesome = TextifyNum();

    setUp(() {
      // Additional setup goes here.
    });

    test('First Test', () {
      expect(awesome.isTextifyNum, isTrue);
    });
  });
}
