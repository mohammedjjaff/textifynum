// TODO: Put public facing types in this file.

/// Checks if you are awesome. Spoiler: you are.
class TextifyNum {
  bool get isTextifyNum => true;


  String convertToArabicWords(int number) {
    List<String> arabicOnes = [
      '', 'واحد', 'اثنان', 'ثلاثة', 'أربعة', 'خمسة', 'ستة', 'سبعة', 'ثمانية', 'تسعة'
    ];

    List<String> arabicTens = [
      '', 'عشرة', 'عشرون', 'ثلاثون', 'أربعون', 'خمسون', 'ستون', 'سبعون', 'ثمانون', 'تسعون'
    ];

    List<String> arabicHundreds = [
      '', 'مئة', 'مئتان', 'ثلاثمئة', 'أربعمئة', 'خمسمئة', 'ستمئة', 'سبعمئة', 'ثمانمئة', 'تسعمئة'
    ];

    if (number == 0) {
      return 'صفر';
    }
    String and = 'و ';

    String words = '';
    if (number < 0) {
      words = 'سالب ';
      number = -number;
    }

    if ((number / 1000000000000).toInt() > 0) {
      words = convertToArabicWords(number ~/ 1000000000000) + ' تريليون ';
      number %= 1000000000000;
    }

    if ((number / 1000000000).toInt() > 0) {
      words = convertToArabicWords(number ~/ 1000000000) + ' مليار ' + words + 'و';
      number %= 1000000000;
    }

    if ((number / 1000000).toInt() > 0) {
      words = convertToArabicWords(number ~/ 1000000) + ' مليون ' + words + 'و';
      number %= 1000000;
    }

    if ((number / 1000).toInt() > 0) {
      words = (number < 3000 ? '' : convertToArabicWords(number ~/ 1000))
          + (number < 2000 ? " الف " : (number > 2000 && number < 3000 ? ' الفان ': " الاف ")) + (number % 1000 == 0 ? '' : and);
      number %= 1000;
    }

    if ((number / 100).toInt() > 0) {
      words =  words + ' ' + arabicHundreds[number ~/ 100] + ((number % 10).toInt() != 0 ? ' و ' : '');
      number %= 100;
    }

    if (number > 0) {
      if (words.isNotEmpty) {
        //words += 'و';
      }

      if (number < 10) {
        words = words + ' ' + arabicOnes[number];
      } else if (number < 20) {
        if(number == 11){
          words = words +  'احد' + ' عشر ';
        }else{
          words = words +  arabicOnes[number - 10] + ' عشر ';
        }
      } else {
        words = words.replaceAll('الاف', 'الف');
        if ((number % 10) > 0) {
          words = words + ' '  + arabicOnes[number % 10];
        }
        words = words + (words.toString() == '' || words.toString() == ' ' ? '' : ' و ')  + arabicTens[number ~/ 10];
      }
    }

    words = words.replaceAll('وو','و').replaceAll('و و','و');

    return words.trim();
  }


}
