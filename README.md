# Web Dashboard

Design an integrated control panel in a simple time.

![Logo](https://storage.googleapis.com/cms-storage-bucket/847ae81f5430402216fd.svg)


## Getting Started

To install, add it to your `pubspec.yaml` file:

```
dependencies:
    textifynum:

```

```dart
import 'package:textifynum/textifynum.dart';
```

## Usage/Examples

```dart
import 'package:textifynum/textifynum.dart';

void main() {
  var textfiyNum = TextifyNum();
  print('textfiy: ${textfiyNum.isTextifyNum}');
}
```


## Support

For support, email info@mohammed-aljaf.com .


## Follow me at Instagram

- Instagram : https://instagram.com/m9_6m?igshid=YTQwZjQ0NmI0OA==

